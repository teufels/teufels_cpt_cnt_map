/**
 * Created by georgkathan on 15/12/15.
 */

var i = 0;
function addMarker (map, lat, lon, location, content, image, infowindow, marker, color, pin, markerBounds) {

	infowindow = new google.maps.InfoWindow({
                 content: infowindow,
                 maxWidth: 500
     });
    /*console.log("i = " + i);*/
    location = location.replace(/&quot;/g, '"');

    if (image == "") {

        //https://developers.google.com/chart/infographics/docs/dynamic_icons?csw=1#pins

        var iBaseX = (pin == "pin_sleft" ? 23 : (pin == "pin_sright" ? 0 : 10));

        var iSizeX = (pin == "pin_sleft" ? 23 : (pin == "pin_sright" ? 23 : 21));
        var iSizeY = (pin == "pin_sleft" ? 33 : (pin == "pin_sright" ? 33 : 34));


        //var image = new google.maps.MarkerImage('typo3conf/ext/fluidcontent_parent/Resources/Public/Icons/ico_pin.png',
        var image = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_xpin_letter&chld=" + pin + "||" + color,
            /* This marker is 129 pixels wide by 42 pixels tall. */
            //new google.maps.Size(28, 37),
            new google.maps.Size(iSizeX, iSizeY),
            /* The origin for this image is 0,0. */
            //new google.maps.Point(0,0),
            new google.maps.Point(0,0),
            /* The anchor for this image is the base of the flagpole at 18,42. */
            new google.maps.Point(iBaseX, iSizeY)
            //new google.maps.Point(14, 35)
        );
    }

    marker = new google.maps.Marker({
        map: map,
        position: {lat: lat, lng: lon},
        title: location,
        icon: image
    });
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    point = new google.maps.LatLng( lat, lon);
    markerBounds.extend(point);

}



//function loadScript() {
//    var script = document.createElement("script");
//    script.type = "text/javascript";
//    script.src = "http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initialize";
//    document.body.appendChild(script);
//}

function loadScript_(url, callback) {

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;

    if (script.readyState) { //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function () {
            callback();
        };
    }

    script.src = url;
    //document.getElementsByTagName("head")[0].appendChild(script);

    // fetch our section element
    var head_ = document.querySelector("head");

    // prepend our script eleemnt to our head element
    head_.insertBefore(script, head_.firstChild);
}

function loadScript(src,callback){

    var script = document.createElement("script");
    script.type = "text/javascript";
    if(callback)script.onload=callback;
    document.getElementsByTagName("head")[0].appendChild(script);
    script.src = src;


}

//window.onload = loadScript;
console.log('asyncMap laoded');
var mapInTabLoader__interval = setInterval(function () {

    if (typeof jQuery == 'undefined' && typeof teufels_cfg_typoscript__windowLoad == 'undefined') {} else {
        clearInterval(mapInTabLoader__interval);


                loadScript_("https://www.google.com/jsapi?.js",function(){

                    google.load("maps", "3",{
                        callback:function(){
                            var mapInTabLoader__interval1 = setInterval(function () {
                                if (typeof google == 'undefined') {} else {
                                    if (typeof google == 'undefined' && typeof google.maps == 'undefined') {} else {
                                        clearInterval(mapInTabLoader__interval1);
                                        //console.log(typeof google.maps);
                                        var abf = [{
                                            featureType: "all",
                                            stylers: [
                                                { "saturation": -100 },
                                                { "hue": "#ed484c" }
                                            ]
                                        }];

                                        var myOptions = {
                                            center: new google.maps.LatLng(mapotions['latCenter'], mapotions['lonCenter']),
                                            zoom: mapotions['zoom'],

                                            //mapTypeId: google.maps.MapTypeId.ROADMAP,
                                            styles: abf,
                                            panControl: false,
                                            mapTypeControl: true,
                                            streetViewControl: false,
                                            overviewMapControl: true,
                                            scaleControl: true,

                                            draggable:  true,
                                            scrollwheel:  true,
                                            disableDoubleClickZoom:  false,
                                            zoomControl:  true,

                                            zoomControlOptions: {
                                                style: google.maps.ZoomControlStyle.DEFAULT
                                            }
                                        };

                                        var pinColor = "007a00";
                                        //var image = new google.maps.MarkerImage('typo3conf/ext/fluidcontent_parent/Resources/Public/Icons/ico_pin.png',
                                        var image = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                                            /* This marker is 129 pixels wide by 42 pixels tall. */
                                            //new google.maps.Size(28, 37),
                                            new google.maps.Size(21, 34),
                                            /* The origin for this image is 0,0. */
                                            //new google.maps.Point(0,0),
                                            new google.maps.Point(0,0),
                                            /* The anchor for this image is the base of the flagpole at 18,42. */
                                            new google.maps.Point(10, 34)
                                            //new google.maps.Point(14, 35)
                                        );

                                        var map = new google.maps.Map(document.getElementById("map"), myOptions);

                                        var markerBounds = new google.maps.LatLngBounds();
										var markercount = 0;
                                        markers.forEach(function(marker) {
										   addMarker (map, marker['lat'], marker['lon'], '', '',  marker['pinOwn'], marker['info'], "marker"+markercount,  marker['mColor'], marker['pin'], markerBounds);
										   markercount ++;
										});
										if (markercount > 1) {
											map.fitBounds(markerBounds);
										}

                                

                        }
                    }
                }, 2000);
                        }
                    });

                });



                //new google.maps.Map(document.getElementById('map'), {
                //    center: new google.maps.LatLng(48.32895, 8.46261),

              
            
        
        $('.map_wrap button.activateMap').unbind('click').bind('click', function () {
            //console.log($(this));
            $('button.activateMap').find('span').each(function () {
                $(this).toggle();
            });
            $('.map_wrap .map_overlay').toggle();
        });
    }

}, 1000);


//$(window).load(function(){
//    $('li.mapInTab a').bind('click',function() {
//        $('div.mapInTab').css('visibility','visible');
//        if (!$('div.mapInTab').hasClass('cAct')) {
//            loadScript();
//            $('div.mapInTab').addClass('cAct');
//        }
//    });
//    $('.map_wrap button.activateMap').bind('click', function(){
//        $(this).find('span').each(function(){
//            $(this).toggle();
//        });
//        $('.map_wrap .map_overlay').toggle();
//    });
//});


